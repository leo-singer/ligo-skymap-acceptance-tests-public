import json as _json

import numpy as np


def define_env(env):
    env.macro(np.format_float_scientific)

    @env.macro
    def json(filename):
        with open(filename) as f:
            return _json.load(f)
