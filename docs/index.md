---
hide:
  - navigation
---

# Acceptance tests for [ligo.skymap](https://git.ligo.org/lscsoft/ligo.skymap)

The baseline results are from BAYESTAR in LALInference 1.9.4.1 (lscsoft/lalsuite@b05391e18175aa2496de895ae46cacd8906f6a32).

## First Two Years: BAYESTAR

### Test description

Run [`bayestar-localize-coincs`](https://lscsoft.docs.ligo.org/ligo.skymap/tool/bayestar_localize_coincs.html) on all 500 events from the ["First Two Years" study](https://www.ligo.org/scientists/first2years/).
These events *do not* have SNR time series, so the test exercises BAYESTAR's code path for synthesizing fake SNR time series using the template autocorrelation function.

### Acceptance criteria

In the five summary figures below, there should be no significant difference between the baseline version and the version under test.

1. [ ] P-P plot in sky position
2. [ ] P-P plot in distance
3. [ ] P-P plot jointly in sky position and distance (volume)
4. [ ] Histogram of searched area
5. [ ] Histogram of angle offset between injected position and maximum a posteriori position

### Results

| 2015                                      | 2016                                      |
| ----------------------------------------- | ----------------------------------------- |
| ![comparison][f2y/15/searched_prob]       | ![comparison][f2y/16/searched_prob]       |
| ![comparison][f2y/15/searched_prob_dist]  | ![comparison][f2y/16/searched_prob_dist]  |
| ![comparison][f2y/15/searched_prob_vol]   | ![comparison][f2y/16/searched_prob_vol]   |
| ![comparison][f2y/15/searched_area_hist]  | ![comparison][f2y/16/searched_area_hist]  |
| ![comparison][f2y/15/offset_hist]         | ![comparison][f2y/16/offset_hist]         |

## First Two Years: Posterior Samples to FITS

### Test description

Run [`ligo-skymap-from-samples`](https://lscsoft.docs.ligo.org/ligo.skymap/tool/ligo_skymap_from_samples.html) on all 500 of the LALInference posterior sample chains from the ["First Two Years" study](https://www.ligo.org/scientists/first2years/).

### Acceptance criteria

In the five summary figures below, there should be no significant difference between the baseline version and the version under test.

1. [ ] P-P plot in sky position
2. [ ] P-P plot in distance
3. [ ] P-P plot jointly in sky position and distance (volume)
4. [ ] Histogram of searched area
5. [ ] Histogram of angle offset between injected position and maximum a posteriori position

### Results

| 2015                                          | 2016                                          |
| --------------------------------------------- | --------------------------------------------- |
| ![comparison][f2y/kde/15/searched_prob]       | ![comparison][f2y/kde/16/searched_prob]       |
| ![comparison][f2y/kde/15/searched_prob_dist]  | ![comparison][f2y/kde/16/searched_prob_dist]  |
| ![comparison][f2y/kde/15/searched_prob_vol]   | ![comparison][f2y/kde/16/searched_prob_vol]   |
| ![comparison][f2y/kde/15/searched_area_hist]  | ![comparison][f2y/kde/16/searched_area_hist]  |
| ![comparison][f2y/kde/15/offset_hist]         | ![comparison][f2y/kde/16/offset_hist]         |

## Historical Events

### Test description

Run [`bayestar-localize-lvalert`](https://lscsoft.docs.ligo.org/ligo.skymap/tool/bayestar_localize_lvalert.html) on the original input data from GraceDB for a selection of past GW alerts.
These events *do* have SNR time series, so the test exercises BAYESTAR's code path for using existing SNR time series.

### Acceptance criteria

1. [ ] The Mollweide plots are pixel-for-pixel identical to the baseline versions (as measured using [`matplotlib.testing.compare.compare_images`](https://matplotlib.org/api/testing_api.html#matplotlib.testing.compare.compare_images)).

### Results

| Event         | Baseline                                              | Latest                                            | [JS Divergence](https://en.wikipedia.org/wiki/Jensen%E2%80%93Shannon_divergence)  | Image Comparison                                                  |
| ------------- | ----------------------------------------------------- | ------------------------------------------------- | --------------------------------------------------------------------------------- | ----------------------------------------------------------------- |
{% for event in 'GW170814 GW170817 GW190707_093326 GW200224_222234 GW200311_115853'.split() -%}
| {{ event }}   | ![baseline]({{ event }}/baseline/bayestar.png)        | ![latest]({{ event }}/output/bayestar.png)        | {{ format_float_scientific(json(event + '/output/divergence.json')['2d'], 2) }}   | ![difference]({{ event }}/output/bayestar-failed-diff.png)        |
|               | ![baseline]({{ event }}/baseline/bayestar.volume.png) | ![latest]({{ event }}/output/bayestar.volume.png) | {{ format_float_scientific(json(event + '/output/divergence.json')['3d'], 2) }}   | ![difference]({{ event }}/output/bayestar.volume-failed-diff.png) |
{% endfor %}

[f2y/15/searched_prob]:             first2years/comparison/2015/searched_prob.svg
[f2y/16/searched_prob]:             first2years/comparison/2016/searched_prob.svg
[f2y/15/searched_prob_dist]:        first2years/comparison/2015/searched_prob_dist.svg
[f2y/16/searched_prob_dist]:        first2years/comparison/2016/searched_prob_dist.svg
[f2y/15/searched_prob_vol]:         first2years/comparison/2015/searched_prob_vol.svg
[f2y/16/searched_prob_vol]:         first2years/comparison/2016/searched_prob_vol.svg
[f2y/15/searched_area_hist]:        first2years/comparison/2015/searched_area_hist.svg
[f2y/16/searched_area_hist]:        first2years/comparison/2016/searched_area_hist.svg
[f2y/15/offset_hist]:               first2years/comparison/2015/offset_hist.svg
[f2y/16/offset_hist]:               first2years/comparison/2016/offset_hist.svg

[f2y/kde/15/searched_prob]:         first2years/kde/comparison/2015/searched_prob.svg
[f2y/kde/16/searched_prob]:         first2years/kde/comparison/2016/searched_prob.svg
[f2y/kde/15/searched_prob_dist]:    first2years/kde/comparison/2015/searched_prob_dist.svg
[f2y/kde/16/searched_prob_dist]:    first2years/kde/comparison/2016/searched_prob_dist.svg
[f2y/kde/15/searched_prob_vol]:     first2years/kde/comparison/2015/searched_prob_vol.svg
[f2y/kde/16/searched_prob_vol]:     first2years/kde/comparison/2016/searched_prob_vol.svg
[f2y/kde/15/searched_area_hist]:    first2years/kde/comparison/2015/searched_area_hist.svg
[f2y/kde/16/searched_area_hist]:    first2years/kde/comparison/2016/searched_area_hist.svg
[f2y/kde/15/offset_hist]:           first2years/kde/comparison/2015/offset_hist.svg
[f2y/kde/16/offset_hist]:           first2years/kde/comparison/2016/offset_hist.svg
