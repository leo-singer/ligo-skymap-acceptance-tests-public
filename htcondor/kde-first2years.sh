#!/bin/sh -ex
OUTDIR=$(mktemp -d)
trap "rm -rf ${OUTDIR}" EXIT
ligo-skymap-from-samples \
    --seed 0 --maxpts 5000 \
    first2years/kde/input/${OBSERVING_RUN}/${COINC_EVENT_ID}.hdf5 \
    --objid coinc_event:coinc_event_id:${COINC_EVENT_ID} \
    -o ${OUTDIR}
mv ${OUTDIR}/skymap.fits first2years/kde/output/${OBSERVING_RUN}/${COINC_EVENT_ID}.fits
ligo-skymap-stats \
    -d first2years/input/${OBSERVING_RUN}/subset.sqlite \
    first2years/kde/output/${OBSERVING_RUN}/${COINC_EVENT_ID}.fits \
    -o first2years/kde/output/${OBSERVING_RUN}/${COINC_EVENT_ID}.dat
