#!/bin/sh -ex
cd ${EVENT_NAME}
bayestar-localize-coincs input/coinc.xml input/psd.xml.gz -o output
ligo-skymap-plot baseline/*.fits* -o baseline/bayestar.png
ligo-skymap-plot output/*.fits* -o output/bayestar.png
ligo-skymap-plot-volume baseline/*.fits* -o baseline/bayestar.volume.png
ligo-skymap-plot-volume output/*.fits* -o output/bayestar.volume.png --align-to baseline/*.fits*
python -c 'from matplotlib.testing.compare import compare_images; print(compare_images("baseline/bayestar.png", "output/bayestar.png", 0))'
python -c 'from matplotlib.testing.compare import compare_images; print(compare_images("baseline/bayestar.volume.png", "output/bayestar.volume.png", 0))'
python ../htcondor/divergence.py baseline/*.fits* output/*.fits* > output/divergence.json
fitsheader output/*.fits* > output/bayestar.txt
