#!/bin/sh -ex
PACKAGE_VERSION=$(python -c 'from ligo.skymap import __version__; print(__version__)')
head -qn 2 $(ls first2years/output/${OBSERVING_RUN}/*.dat | head -n 1) > first2years/comparison/${OBSERVING_RUN}/${PACKAGE_VERSION}.dat
tail -qn -1 first2years/output/${OBSERVING_RUN}/*.dat >> first2years/comparison/${OBSERVING_RUN}/${PACKAGE_VERSION}.dat
ligo-skymap-plot-stats \
    --format svg \
    first2years/comparison/${OBSERVING_RUN}/baseline.dat \
    first2years/comparison/${OBSERVING_RUN}/${PACKAGE_VERSION}.dat \
    -o first2years/comparison/${OBSERVING_RUN}
