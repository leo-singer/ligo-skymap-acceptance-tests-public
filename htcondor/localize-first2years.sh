#!/bin/sh -ex
bayestar-localize-coincs \
    first2years/input/${OBSERVING_RUN}/subset.xml \
    -o first2years/output/${OBSERVING_RUN} \
    --coinc-event-id ${COINC_EVENT_ID} \
    --f-low ${F_LOW}
ligo-skymap-stats \
    -d first2years/input/${OBSERVING_RUN}/subset.sqlite \
    first2years/output/${OBSERVING_RUN}/${COINC_EVENT_ID}.fits \
    -o first2years/output/${OBSERVING_RUN}/${COINC_EVENT_ID}.dat \
    -p 20 50 90
